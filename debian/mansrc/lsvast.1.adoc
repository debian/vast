# lsvast(1)

## NAME

lsvast - show VAST information

## SYNOPSIS

*lsvast* <path/to/vast.db> [options]

## OPTIONS

*--verbose*::
  Show verbose output.

*--human-readable*::
  Provide human readable output.

*--print-bytesizes*::
  Print byte sizes.

## AUTHORS

Tenzir GmbH

## ADDITIONAL DOCUMENTATION

Visit <http://vast.io> for more information about VAST.